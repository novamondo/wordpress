<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jmus');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A_/VdK<0 0 FrX;qKtwceVt,pk }VPu*yegYh @p}qPDEd@om$bjQ WGHvR]oIw7');
define('SECURE_AUTH_KEY',  'o5I$De)*B^.XhCbDC_j%CsYv *bv@JdeEy;Tn}+3F]aC;Q:[PwY-:EQ&$mp7l]lP');
define('LOGGED_IN_KEY',    's+|PDG^q~=`F`+Rnt=)DwtFEa /5<4+$L{a#C!g%?w)m5vxTX60ESmE3.1.&jFjt');
define('NONCE_KEY',        'nTV0J]gx~ej_X(^#SkmlkmYBKA_h+`.~_likd.3zpEYy%5WX:.uE<G$m?|75V/Xu');
define('AUTH_SALT',        'pT$sFI%>`$djSjYTW)wyT~ANJ75a^7?q*Ei2Qz+_jO::4L]=]v2C2xKXqAR16( ~');
define('SECURE_AUTH_SALT', '7z[jMewW%/EXQ=~{!!W-n(`-_F>Y^`,#X8>-{QjdY~@>OIWm@}zd.z-B|a8V5>%J');
define('LOGGED_IN_SALT',   '1<RwA&O0J0%BTVXQe6epCZYu5-A6D|SfD~72caw#DYfwbPsi1ci~R2ypR3wVy&e^');
define('NONCE_SALT',       '7CP1$;$]`K]O,+v1lZ2#{hx}G_A=C?Yg-3w>3|!%Ct%>rx7DuK5^]#3tRh~TZGX?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
